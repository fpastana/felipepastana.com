@extends('layouts.master')

@section('conteudo')

<!-- Container -->
<div class="content-wrap">

   <section id="homesection" class="container-fluid nopadding">

      <div class="m-details row nopadding bg-color01">

         <div class="col-md-8 nopadding">
            <div class="padding-50 wow fadeInDown" data-wow-delay="0.2s" data-wow-offset="10">
               <div class="row nopadding">
                  <div class="col-md-12 nopadding">
                     <h3 class="font-accident-two-normal uppercase">Sobre mim</h3>
                     <div class="quote">
                        <h5 class="font-accident-one-bold uppercase subtitle">Trabalhando pesado rumo ao sucesso...</h5>
                        <div class="dividewhite1"></div>
                        <!--<p class="small">-->
                           <!--Customize your website as you want using different colors and 100% free fonts.-->
                        <!--</p>-->
                        <div class="dividewhite2"></div>
                     </div>
                  </div>
               </div>
               <div class="divider-dynamic"></div>
               <div class="row nopadding">
                  <div class="col-md-4 infoblock nopadding">
                     <div class="row">
                        <div class="col-sm-1 col-md-3"><i class="flaticon-circle-1"></i><div class="dividewhite1"></div></div>
                        <div class="col-sm-11 col-md-9">
                           <h5 class="font-accident-one-bold uppercase">Comprometido</h5>
                           <p class="small">
                              Dedicado e comprometido, procuro estar sempre oferecendo o melhor feedback para meus clientes.
                           </p>
                        </div>
                     </div>
                     <div class="divider-dynamic"></div>
                  </div>
                  <div class="col-md-4 infoblock nopadding">
                     <div class="row">
                        <div class="col-sm-1 col-md-3"><i class="flaticon-book-bag2"></i><div class="dividewhite1"></div></div>
                        <div class="col-sm-11 col-md-9">
                           <h5 class="font-accident-one-bold uppercase">Proativo</h5>
                           <p class="small">
                              Procuro estar sempre atualizado e trazendo o que há de mais moderno, seja lendo artigos ou fazendo cursos.
                           </p>
                        </div>
                     </div>
                     <div class="divider-dynamic"></div>
                  </div>
                  <div class="col-md-4 infoblock nopadding">
                     <div class="row">
                        <div class="col-sm-1 col-md-3"><i class="flaticon-stats47"></i><div class="dividewhite1"></div></div>
                        <div class="col-sm-11 col-md-9">
                           <h5 class="font-accident-one-bold uppercase">Ético</h5>
                           <p class="small">
                              Trabalho com o máximo de seriedade, sempre respeitando as regras estabelcidas com o máximo de honestidade.
                           </p>
                        </div>
                     </div>
                     <div class="divider-dynamic"></div>
                  </div>
               </div>
            </div>
         </div>

         <div class="col-md-4 personal nopadding ui-block-color01">
            <div class="padding-50 wow fadeInRight" data-wow-delay="0.4s" data-wow-offset="10">
               <h3 class="font-accident-two-normal uppercase">Informações Pessoais</h3>
               <div class="dividewhite3"></div>
               <div>
                  <div class="fullwidth box">
                     <div class="one"><p class="small uppercase">Nome:</p></div>
                     <div class="two"><p class="small">Felipe Pastana</p></div>
                  </div>
                  <div class="fullwidth box">
                     <div class="one"><p class="small uppercase text-nowrap">Nascimento:</p></div>
                     <div class="two"><p class="small">24/02/1988</p></div>
                  </div>
                  <div class="fullwidth box">
                     <div class="one"><p class="small uppercase">Skype:</p></div>
                     <div class="two"><p class="small">felipe_pastana</p></div>
                  </div>
                  <div class="fullwidth box">
                     <div class="one"><p class="small uppercase">Email:</p></div>
                     <div class="two"><p class="small">felipeapastana@gmail.com</p></div>
                  </div>
               </div>
               <div class="dividewhite1"></div>
            </div>
         </div>

      </div>

      <div class="row nopadding ui-block-color02">

         <div class="col-md-4 pro-experience nopadding">
            <div class="padding-50 wow fadeInRight" data-wow-delay="0.6s" data-wow-offset="5">
               <h3 class="font-accident-two-normal uppercase">Experiência Profissional</h3>
               <div class="dividewhite4"></div>
               <div class="experience">
                  <ul class="">
                     <li class="date">02/2016 - hoje</li>
                     <li class="company uppercase">
                        <a>
                           Pirâmide Sea Air Comércio Exterior Ltda.
                        </a>
                     </li>
                     <li class="position">Analista Programador de Sistemas Web</li>
                  </ul>
                  <ul class="">
                     <li class="date">04/2015 - 02/2016</li>
                     <li class="company uppercase">
                        <a>
                           Tim Fiber
                        </a>
                     </li>
                     <li class="position">Analista Programador PHP Senior</li>
                  </ul>
                  <ul class="">
                     <li class="date">10/2011 - 04/2015</li>
                     <li class="company uppercase">
                        <a>
                           Haidar Administradora de Comércio Exterior
                        </a>
                     </li>
                     <li class="position">Analista Programador de Sistemas</li>
                  </ul>
               </div>
               <a href="#!" class="btn btn-wh-trans btn-xs">Veja Mais</a>
               <div class="dividewhite1"></div>
            </div>
         </div>

         <div class="col-md-8 circle-skills nopadding ui-block-color03">
            <div class="padding-50 wow fadeInLeft" data-wow-delay="0.6s" data-wow-offset="5">
               <h3 class="font-accident-two-normal uppercase">Minhas Habilidades Profissionais</h3>
               <div class="dividewhite1"></div>
               <div class="row">
                  <div class="col-sm-4 nopadding">
                     <div class="progressbar" data-animate="false">
                        <div class="circle font-accident-one-normal" data-percent="75.5">
                           <div></div>
                           <h4 class="font-accident-one-normal uppercase">Banco de Dados</h4>
                           <p class="font-regular-normal">
                              Habilidade de administrar e otimizar principalmente bancos de dados SQL ...
                           </p>
                           <a href="#!" class="btn btn-wh-trans btn-xs">Veja Mais</a>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-4 nopadding">
                     <div class="progressbar" data-animate="false">
                        <div class="circle font-accident-one-normal" data-percent="94.5">
                           <div></div>
                           <h4 class="font-accident-one-normal uppercase">Programação</h4>
                           <p class="font-regular-normal">
                              Dominando principalmente o PHP, junto ao Laravel. Sempre utilizando POO ...
                           </p>
                           <a href="#!" class="btn btn-wh-trans btn-xs">Veja Mais</a>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-4 nopadding">
                     <div class="progressbar" data-animate="false">
                        <div class="circle font-accident-one-normal" data-percent="30.5">
                           <div></div>
                           <h4 class="font-accident-one-normal uppercase">UML</h4>
                           <p class="font-regular-normal">
                              Habilidade de documentar os sistemas para uma maior organização ...
                           </p>
                           <a href="#!" class="btn btn-wh-trans btn-xs">Veja Mais</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

      </div>

   </section>

</div>

@stop