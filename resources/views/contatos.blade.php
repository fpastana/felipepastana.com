@extends('layouts.master')

@section('conteudo')
<!-- Container -->
<div class="content-wrap">

   <div id="contacts" class="inner-content">

      <section id="page-title" class="inner-section">
         <div class="container-fluid nopadding wow fadeInRight" data-wow-delay="0.4s" data-wow-offset="10">
            <h2 class="font-accident-two-normal uppercase">Contatos</h2>
            <h5 class="font-accident-one-bold uppercase subtitle">Trabalhando pesado rumo ao sucesso...</h5>
            <p class="small fontcolor-medium">
               Olá, me chamo Felipe Pastana e trabalho como freelancer fazendo o que amo à noite e nos finais de semana. Sou especilista em desenvolvimento web, tendo como foco a linguagem de programação PHP. Caso tenha interesse em fazer um orçamento para qualquer tipo de sistema web, basta entrar em contato comigo por qualquer contato abaixo.
            </p>
         </div>
         <div class="dividewhite4"></div>
      </section>

      <section id="contacts-data" class="inner-block">
         <div class="container-fluid nopadding">
            <div class="row">
               <div class="col-md-6 wow fadeInLeft" data-wow-delay="0.6s" data-wow-offset="10">
                  <div class="row">
                     <div class="col-sm-2"><span class="font-accident-two-bold uppercase">Cidade:</span></div>
                     <div class="col-sm-10"><p class="small">São Paulo - SP</p></div>
                  </div>
                  <div class="row">
                     <div class="col-sm-2"><span class="font-accident-two-bold uppercase">Telefone:</span></div>
                     <div class="col-sm-10"><p class="small">@felipe_pastana</p></div>
                  </div>
                  <div class="row">
                     <div class="col-sm-2"><span class="font-accident-two-bold uppercase">Skype:</span></div>
                     <div class="col-sm-10"><p class="small">@felipe_pastana</p></div>
                  </div>
               </div>
               <div class="col-md-6 wow fadeInLeft" data-wow-delay="0.6s" data-wow-offset="10">
                  <div class="row">
                     <div class="col-sm-2"><span class="font-accident-two-bold uppercase">Twitter:</span></div>
                     <div class="col-sm-10"><p class="small"><a href="#!">https://twitter.com/felipe_pastana</a></p></div>
                  </div>
                  <div class="row">
                     <div class="col-sm-2"><span class="font-accident-two-bold uppercase">Facbook:</span></div>
                     <div class="col-sm-10"><p class="small"><a href="#!">https://www.facebook.com/felipe.pastana</a></p></div>
                  </div>
                  <div class="row">
                     <div class="col-sm-2"><span class="font-accident-two-bold uppercase">Linkedin:</span></div>
                     <div class="col-sm-10"><p class="small"><a href="#!">https://br.linkedin.com/in/felipe-pastana-4b553219</a></p></div>
                  </div>
               </div>
            </div>
         </div>
      </section>
       
       <center>
      <!-- Google Map Block -->
      <section id="contacts-map" class="inner-section">
         <div class="container-fluid nopadding">
            <!-- Google Map -->
            <div class="wow fadeInDown" data-wow-delay="0.8s" data-wow-offset="10">
               <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d467768.5643664614!2d-46.81369047338952!3d-23.660149702542846!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce448183a461d1%3A0x9ba94b08ff335bae!2zU8OjbyBQYXVsbywgU1A!5e0!3m2!1spt-BR!2sbr!4v1469500102214" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <!-- /Google Map -->
         </div>
         <div class="dividewhite8"></div>
      </section>
      <!-- /Google Map Block -->
       </center>

   </div>

</div>
@stop