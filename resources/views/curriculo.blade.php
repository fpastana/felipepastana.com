@extends('layouts.master')

@section('conteudo')
<!-- Container -->
<div class="content-wrap">

   <div id="resume" class="inner-content">

      <section id="page-title" class="inner-section">
         <div class="container-fluid nopadding wow fadeInRight" data-wow-delay="0.2s" data-wow-offset="10">
            <h2 class="font-accident-two-normal uppercase">Currículo</h2>
            <h5 class="font-accident-one-bold uppercase subtitle">Trabalhando pesado rumo ao sucesso...</h5>
            <p class="small fontcolor-medium">
               Atuo na área de Análise e Programação Web / Mobile, construindo websites, emails, sistemas, ou qualquer projeto voltado para web, dominando a linguagem PHP 5.6 OO, MVC, HTML, CSS, XML, jQuery e Json, trabalhando diretamente na criação de módulos back-end. Podendo também administrar rotinas SQL, para otimização das mesmas, utilizando técnicas de normalização e indexação em SGBDs como: PostgreSQL, MySQL, Firebird e SQL Server. Conhecimentos intermediários em Java para desenvolvimento de aplicações mobile (Android).
            </p>
            <div class="dividewhite2"></div>
         </div>
      </section>

      <!-- Details Block -->
      <section id="m-details" class="inner-section bg-color02">

         <div class="container-fluid nopadding">

            <div class="wow fadeInDown" data-wow-delay="0.4s" data-wow-offset="10">
               <h3 class="font-accident-two-normal uppercase text-center">Qualidades Pessoais</h3>
               <p class="small text-center fontcolor-medium">
                  Abaixo segue as principais competências e qualidades que pude captar de depoimentos de colegas de trabalho.
               </p>
               <div class="dividewhite4"></div>
            </div>



            <div class="row">
               <div class="col-md-3 infoblock text-center wow fadeInLeft" data-wow-delay="0.6s" data-wow-offset="10">
                  <div class="row">
                     <div class="col-md-12"><i class="flaticon-arrows-2"></i></div>
                     <div class="col-md-12">
                        <div class="dividewhite1"></div>
                        <h5 class="font-accident-one-bold uppercase">Resiliente</h5>
                        <p class="small">
                           Encaro os desafios de forma otimista, sempre buscando encontrar a solução para cada problema.
                           Procuro estar sempre produzindo e fazendo o meu melhor.
                        </p>
                     </div>
                  </div>
                  <div class="divider-dynamic"></div>
               </div>
               <div class="col-md-3 infoblock text-center wow fadeInUp" data-wow-delay="0.6s" data-wow-offset="10">
                  <div class="row">
                     <div class="col-md-12"><i class="flaticon-circle-4"></i></div>
                     <div class="col-md-12">
                        <div class="dividewhite1"></div>
                        <h5 class="font-accident-one-bold uppercase">Apaixonado</h5>
                        <p class="small">
                           Desde cedo sou fascinado pela área de T.I. e a cada dia que passa busco novas soluções
                           para levar sempre o que há de mais moderno aos meus clientes.
                        </p>
                     </div>
                  </div>
                  <div class="divider-dynamic"></div>
               </div>
               <div class="col-md-3 infoblock text-center wow fadeInUp" data-wow-delay="0.6s" data-wow-offset="10">
                  <div class="row">
                     <div class="col-md-12"><i class="flaticon-photography39"></i></div>
                     <div class="col-md-12">
                        <div class="dividewhite1"></div>
                        <h5 class="font-accident-one-bold uppercase">Espírito amigável</h5>
                        <p class="small">
                           Procuro sempre me adptar à cultura organizacional, para sempre manter um ótimo relacionamento com 
                           as pessoas, visando manter a ordem e consequentimente o progresso da organização.
                        </p>
                     </div>
                  </div>
                  <div class="divider-dynamic"></div>
               </div>
               <div class="col-md-3 infoblock text-center wow fadeInRight" data-wow-delay="0.6s" data-wow-offset="10">
                  <div class="row">
                     <div class="col-md-12"><i class="flaticon-stats47"></i></div>
                     <div class="col-md-12">
                        <div class="dividewhite1"></div>
                        <h5 class="font-accident-one-bold uppercase">Inovador</h5>
                        <p class="small">
                           Por estar constantemente trazendo novas soluções, os clientes estão sempre satisfeitos com os projetos,
                           sempre trazendo novas demandas. 
                        </p>
                     </div>
                  </div>
                  <div class="divider-dynamic"></div>
               </div>
            </div>

            <div class="dividewhite2"></div>

         </div>

      </section>
      <!-- /§Details Block -->

      <!-- Timeline Block -->
      <section id="timeline-vertical" class="inner-section">

         <div class="container-fluid nopadding">

            <div class="text-center wow fadeInDown" data-wow-delay="0.6s" data-wow-offset="10">
               <div class="dividewhite4"></div>
               <h3 class="font-accident-two-normal uppercase">Timeline Profissional</h3>
               <h5 class="font-accident-one-bold uppercase subtitle">Trabalhando pesado rumo ao sucesso</h5>
               <div class="dividewhite1"></div>
               <p class="small fontcolor-medium">
                  Abaixo seguem algumas das empresas que já tive o privilégio de trabalhar. Pude crescer muito profissionalemnte com cada projeto pego.
                  <br>
                  "Perder tempo em aprender coisas que não interessam, priva-nos de descobrir coisas interessantes." (Carlos Drummond de Andrade)
               </p>
            </div>

            <div class="dividewhite4"></div>

            <ul class="timeline-vert timeline-light">
               <li>
                  <div class="timeline-badge primary"><i class="flaticon-placeholders4"></i></div>
                  <div class="timeline-panel wow fadeInLeft" data-wow-delay="0.3s" data-wow-offset="10">
                     <p class="timeline-time fontcolor-invert"><i class="glyphicon glyphicon-time"></i> Fev 2016 - ...</p>
                     <div class="timeline-photo timeline-bg01-01"></div>
                     <div class="timeline-heading">
                        <h4 class="font-accident-two-normal uppercase">Pirâmide Sea Air Comércio Exterior LTDA.</h4>
                        <h6 class="uppercase">Analista Programador de Sistemas Web</h6>
                     </div>
                  </div>
               </li>
               <li class="timeline-inverted">
                  <div class="timeline-badge success"><i class="flaticon-pens15"></i></div>
                  <div class="timeline-panel wow fadeInRight" data-wow-delay="0.3s" data-wow-offset="10">
                     <p class="timeline-time fontcolor-invert"><i class="glyphicon glyphicon-time"></i> Abr 2015 - Fev 2016</p>
                     <div class="timeline-photo timeline-bg02-01"></div>
                     <div class="timeline-heading">
                        <h4 class="font-accident-two-normal uppercase">Tim Fiber</h4>
                        <h6 class="uppercase">Analista Programador PHP Senior</h6>
                     </div>
                  </div>
               </li>
               <li>
                  <div class="timeline-badge danger"><i class="flaticon-book-bag2"></i></div>
                  <div class="timeline-panel wow fadeInLeft" data-wow-delay="0.3s" data-wow-offset="10">
                     <p class="timeline-time fontcolor-invert"><i class="glyphicon glyphicon-time"></i> Out 2011 - Abr 2015</p>
                     <div class="timeline-photo timeline-bg03-01"></div>
                     <div class="timeline-heading">
                        <h4 class="font-accident-two-normal uppercase">Haidar Administradora de Comércio Exterior</h4>
                        <h6 class="uppercase">Analista Programador de Sistemas Web</h6>
                     </div>
                  </div>
               </li>
               <li class="timeline-inverted info">
                  <div class="timeline-badge warning"><i class="flaticon-profile5"></i></div>
                  <div class="timeline-panel wow fadeInRight" data-wow-delay="0.3s" data-wow-offset="10">
                     <p class="timeline-time fontcolor-invert"><i class="glyphicon glyphicon-time"></i> Out 2008 - Set 2009</p>
                     <div class="timeline-photo timeline-bg04-01"></div>
                     <div class="timeline-heading">
                        <h4 class="font-accident-two-normal uppercase">Tim Norte Celular</h4>
                        <h6 class="uppercase">Analista Programador de Sistemas</h6>
                     </div>
                  </div>
               </li>
               <li>
                  <div class="timeline-badge danger"><i class="flaticon-graduation61"></i></div>
                  <div class="timeline-panel wow fadeInLeft" data-wow-delay="0.3s" data-wow-offset="10">
                     <p class="timeline-time fontcolor-invert"><i class="glyphicon glyphicon-time"></i> Abr 2007 - Out 2008</p>
                     <div class="timeline-photo timeline-bg05-01"></div>
                     <div class="timeline-heading">
                        <h4 class="font-accident-two-normal uppercase">Museu Paraense Emílio Goeldi</h4>
                        <h6 class="uppercase">Estagiário de Sistemas Web</h6>
                     </div>
                  </div>
               </li>
            </ul>
            
            <div class="text-center">
               <a href="#!" class="btn btn-darker">Estudos Superiores</a>
            </div>

            <div class="dividewhite6"></div>

         </div>

      </section>
      <!-- /Timeline Block -->

   </div>

</div>
@stop