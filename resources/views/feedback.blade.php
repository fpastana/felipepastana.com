@extends('layouts.master')

@section('conteudo')
<!-- Container -->
<div class="content-wrap">

   <div id="feedback" class="inner-content">

      <section id="page-title" class="inner-section">
         <div class="container-fluid nopadding wow fadeInRight" data-wow-delay="0.4s" data-wow-offset="10">
            <h2 class="font-accident-two-normal uppercase">Feedback</h2>
            <h5 class="font-accident-one-bold uppercase subtitle">Trabalhando pesado rumo ao sucesso...</h5>
            <p class="small fontcolor-medium">
               Caso tenha alguma dúvida, ou queira dar seu feedback em relação à algum trabalho já realizado, ou ainda gostaria de fazer alguma pergunta, basta entrar em contato comigo pelo formulário abaixo.
            </p>
         </div>
      </section>

      <!-- Feedback Block -->
      <section class="inner-section feedback feedback-light">

         <div class="container-fluid nopadding">
             
            <div class="row">

               <div class="col-md-2">&nbsp;</div>
               <div class="col-md-8 e-centered wow fadeInDown" data-wow-delay="0.6s" data-wow-offset="10">
                   @if(session('status'))
                   <h5 class="font-accident-one-bold uppercase subtitle">{{session('status')}}</h5>
                   @endif
                   
                   <div class="dividewhite2"></div>
                  <div id="form-messages"></div>
                  <form method="post" action="" class="wpcf7-form">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                     <div class="field">
                        <!--<label for="name">Name:</label>-->
                        <input type="text" id="name" name="nome" placeholder="Nome" required>
                     </div>

                     <div class="field">
                        <!--<label for="email">Email:</label>-->
                        <input type="email" id="email" name="email" placeholder="Email" required>
                     </div>

                     <div class="field">
                        <!--<label for="message">Message:</label>-->
                        <textarea id="message" name="mensagem" placeholder="Mensagem" rows="7" cols="30"  required></textarea>
                     </div>

                     <div class="dividewhite2"></div>

                     <div class="field text-center">
                        <button type="submit" class="btn btn-lg btn-darker">Enviar Mensagem</button>
                     </div>
                  </form>
               </div>
               <div class="col-md-2">&nbsp;</div>
               <div class="col-md-12 divider-dynamic"></div>
            </div>

         </div>

         <div class="dividewhite6"></div>

      </section>
      <!-- /Feedback Block -->

      <!-- Testmonials Block -->
      <section id="testmonials" class="inner-section bg-color02">
         <div class="container-fluid nopadding">

            <div class="wow fadeInDown" data-wow-delay="0.4s" data-wow-offset="10">
               <h3 class="font-accident-two-normal uppercase text-center">Depoimentos</h3>
               <h5 class="font-accident-one-bold uppercase text-center subtitle">Trabalhando pesado rumo ao sucesso</h5>
               <div class="dividewhite1"></div>
               <p class="small fontcolor-medium text-center">
                  Agradeço a Deus em primeiro lugar pelo dom que me deu e por tudo o que faz em minha vida, pois sem ele nada seria. <br>
                  Creia em Jesus e serás salvo, pois Ele é o caminho, a verdade e a vida, ninguém vai ao Pai, senão pelo filho. João 14:6.
               </p>
            </div>

            <div class="dividewhite4"></div>

            <div class="row">
               <div class="col-md-4 wow fadeInLeft" data-wow-delay="0.5s" data-wow-offset="10">
                  <div class="row">
                     <div class="col-xs-3">
                        <img src="assets/custom/images/wellington.jpg" alt="Rachel James Johnes" class="img-responsive img-circle author-userpic">
                     </div>
                     <div class="col-xs-9">
                        <h5 class="font-accident-one-bold text-left uppercase">Wellington Santos</h5>
                        <p class="small hovercolor">Diretor na Tribosys</p>
                        <p class="text-left small">
                           Felipe Pastana é um profissional focado e determinado, com uma excelente experiencia técnica. Na equipe é um excelente líder...
                        </p>
                     </div>
                  </div>
                  <div class="divider-dynamic"></div>
               </div>
               <div class="col-md-4 wow fadeInUp" data-wow-delay="0.5s" data-wow-offset="10">
                  <div class="row">
                     <div class="col-xs-3">
                        <img src="assets/custom/images/maximino.jpg" alt="Rachel James Johnes" class="img-responsive img-circle author-userpic">
                     </div>
                     <div class="col-xs-9">
                        <h5 class="font-accident-one-bold text-left uppercase">Maximino Pingarilho</h5>
                        <p class="small hovercolor">Gerente de Desenvolvimento na Montreal</p>
                        <p class="text-left small">
                           Felipe é um profissional competente e dedicado. Apresenta excelentes resultados em seus projetos e surpreende em cada detalhe os seus trabalhos.
                        </p>
                     </div>
                  </div>
                  <div class="divider-dynamic"></div>
               </div>
               <div class="col-md-4 wow fadeInRight" data-wow-delay="0.5s" data-wow-offset="10">
                  <div class="row">
                     <div class="col-xs-3">
                        <img src="assets/custom/images/sol.jpg" alt="Rachel James Johnes" class="img-responsive img-circle author-userpic">
                     </div>
                     <div class="col-xs-9">
                        <h5 class="font-accident-one-bold text-left uppercase">Marisol Moreno</h5>
                        <p class="small hovercolor">Diretora da Sol Moreno Moda Praia</p>
                        <p class="text-left small">
                           Trabalho com o Felipe há anos e posso garantir que ele sempre entrega suas demandas com a melhor qualidade possível. Estou sempre muito satisfeita com seus serviços e recomendo a todos.
                        </p>
                     </div>
                  </div>
                  <div class="divider-dynamic"></div>
               </div>
            </div>

            <div class="dividewhite6"></div>

         </div>
      </section>
      <!-- /Testmonials Block -->

   </div>

</div>
@stop