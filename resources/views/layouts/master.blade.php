<!DOCTYPE html>
<html lang="pt-BR">
<!--[if IE 9]>
<html class="ie9" lang="en">    <![endif]-->
<!--[if IE 8]>
<html class="ie8" lang="en">    <![endif]-->
@section('cabecalho')
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name=viewport content="width=device-width, initial-scale=1">

   <title>FelipePastana | Freelancer</title>

   <meta name="description" content="Currículo de Felipe Pastana - Freelancer profissional com mais de 10 anos de experiência">
   <meta name="keywords" content="html5, template, website, responsive, bootstrap, felipe, pastana">
   <meta name="author" content="felipepastana.com">

   <!-- CSS -->
   <link href="assets/vendor/bootstrap/css/bootstrap.min.css"        property='stylesheet' rel="stylesheet" type="text/css" media="screen"/>
   <link href="assets/vendor/fontawesome/css/font-awesome.min.css"   property='stylesheet' rel="stylesheet" type="text/css" media="screen"/>
   <link href="assets/vendor/flaticons/flaticon.css"                 property='stylesheet' rel="stylesheet" type="text/css" media="screen"/>
   <link href="assets/vendor/hover/css/hover-min.css"                property='stylesheet' rel="stylesheet" type="text/css" media="screen"/>
   <link href="assets/vendor/wow/animate.css"                        property='stylesheet' rel="stylesheet" type="text/css" media="screen"/>
   <link href="assets/vendor/mfp/css/magnific-popup.css"             property='stylesheet' rel="stylesheet" type="text/css" media="screen"/>
   <!-- Custom styles -->
   <link href="assets/custom/css/style.css"                          property='stylesheet' rel="stylesheet" type="text/css" media="screen"/>

   <style>
      #preloader {
         position: fixed;
         left: 0;
         top: 0;
         z-index: 99999;
         width: 100%;
         height: 100%;
         overflow: visible;
         background: #666666 url("assets/custom/images/preloader.gif") no-repeat center center; }
   </style>

</head>
@show

<body class="boxed">

<!--Pre-Loader-->
<div id="preloader"></div>

@section('menu')
<header>
    
    
   <section id="top-navigation" class="container-fluid nopadding">
      <div class="row nopadding ident ui-bg-color01">
         <!-- Photo -->
         <a href="#!">
            <div class="col-md-5 col-lg-4 vc-photo">&nbsp;</div>
         </a>
         <!-- /Photo -->
         <div class="col-md-7 col-lg-8 vc-name nopadding">
            <!-- Name-Position -->
            <div class="row nopadding name">
               <div class="col-md-10 name-title"><h2 class="font-accident-two-light uppercase">Felipe A Pastana</h2></div>
               <div class="col-md-2 nopadding name-pdf">
                  <a href="{{url('downloads/curriculum-felipe-pastana.docx')}}" class="hvr-sweep-to-right"><i class="flaticon-download149" title="Download CV.pdf"></i></a>
               </div>
            </div>
            <div class="row nopadding position">
               <div class="col-md-10 position-title">

                  <section class="cd-intro">
                     <h4 class="cd-headline clip is-full-width font-accident-two-normal uppercase">
                        <span>Freelancer em</span>
                        <span class="cd-words-wrapper">
                           <b class="is-visible">PHP</b>
                           <b>Banco de Dados</b>
                           <b>WebMaster</b>
                        </span>
                     </h4>
                  </section>

               </div>
               <div class="col-md-2 nopadding pdf">
                   <a target="_blank" href="https://about.me/felipepastana" class="hvr-sweep-to-right"><i class="flaticon-identification28" title="Sobre Mim"></i></a>
               </div>
            </div>
            <!-- /Name-Position -->

            <!-- Main Navigation -->

            <ul id="nav" class="row nopadding cd-side-navigation">
               <li class="col-xs-4 col-sm-2 nopadding menuitem ui-menu-color01">
                  <a href="{{url('/')}}" class="hvr-sweep-to-bottom"><i class="flaticon-insignia"></i><span>home</span></a>
               </li>
               <li class="col-xs-4 col-sm-2 nopadding menuitem ui-menu-color02">
                  <a href="{{url('curriculo')}}" class="hvr-sweep-to-bottom"><i class="flaticon-graduation61"></i><span>currículo</span></a>
               </li>
               <li class="col-xs-4 col-sm-2 nopadding menuitem ui-menu-color03">
                  <a href="{{url('portfolio')}}" class="hvr-sweep-to-bottom"><i class="flaticon-book-bag2"></i><span>portfolio</span></a>
               </li>
               <li class="col-xs-4 col-sm-2 nopadding menuitem ui-menu-color04">
                  <a href="{{url('contatos')}}" class="hvr-sweep-to-bottom"><i class="flaticon-placeholders4"></i><span>contatos</span></a>
               </li>
               <li class="col-xs-4 col-sm-2 nopadding menuitem ui-menu-color05">
                  <a href="{{url('feedback')}}" class="hvr-sweep-to-bottom"><i class="flaticon-earphones18"></i><span>feedback</span></a>
               </li>
               <li class="col-xs-4 col-sm-2 nopadding menuitem ui-menu-color06">
                  <a href="#" class="hvr-sweep-to-bottom"><i class="flaticon-pens15"></i><span>blog</span></a>
               </li>
            </ul>

            <!-- /Main Navigation -->

         </div>
      </div>
   </section>
    

</header>
@show

@yield('conteudo')

@section('rodape')
<footer class="padding-50 ui-bg-color02">
   <div class="container-fluid nopadding">
      <div class="row wow fadeInDown" data-wow-delay=".2s" data-wow-offset="10">
         <div class="col-md-3">
            <h4 class="font-accident-two-normal uppercase">Felipe A Pastana</h4>
            <p class="inline-block">
               Programador Especialista, PHP/Laravel. Freelancer.
            </p>
            <div class="divider-dynamic"></div>
         </div>
         <div class="col-md-3 cv-link">
            <h4 class="font-accident-two-normal uppercase">Download cv</h4>
            <div class="dividewhite1"></div>
            <a href="{{url('downloads/curriculum-felipe-pastana.docx')}}"><i class="fa fa-long-arrow-down" aria-hidden="true"></i>Português</a>
            <a href="#!"><i class="fa fa-long-arrow-down" aria-hidden="true"></i>Inglês</a>
            <div class="divider-dynamic"></div>
         </div>
         <div class="col-md-3">
            <h4 class="font-accident-two-normal uppercase">Newsletter</h4>
            <div class="dividewhite1"></div>
            <input class="newsletter-email" type="email" required="" name="ne" placeholder="Seu Email">
            <!--<p>Lorem ipsum dolor sit amet...</p>-->
            <div class="divider-dynamic"></div>
         </div>
         <div class="col-md-3">
            <h4 class="font-accident-two-normal uppercase">Follow me</h4>
            <div class="follow">
               <ul class="list-inline social">
                  <li><a target="_blank" href="https://www.facebook.com/felipe.pastana" class="rst-icon-facebook"><i class="fa fa-facebook"></i></a></li>
                  <li><a target="_blank" href="https://twitter.com/felipe_pastana" class="rst-icon-twitter"><i class="fa fa-twitter"></i></a></li>
                  <li><a target="_blank" href="https://br.linkedin.com/in/felipe-pastana-4b553219" class="rst-icon-linkedin"><i class="fa fa-linkedin"></i></a></li>
               </ul>
            </div>
            <div class="divider-dynamic"></div>
         </div>
      </div>
      <div class="dividewhite1"></div>
      <div class="row wow fadeInDown" data-wow-delay=".4s" data-wow-offset="10">
         <div class="col-md-12 copyrights">
            <p>© 2016 Felipe A Pastana.</p>
         </div>
      </div>
   </div>
</footer>
@show

<div id="image-cache" class="hidden"></div>

<!-- JS -->
<script src="assets/vendor/jquery/js/jquery-2.2.0.min.js"            type="text/javascript"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.min.js"            type="text/javascript"></script>
<script src="assets/vendor/imagesloaded/js/imagesloaded.pkgd.min.js" type="text/javascript"></script>
<script src="assets/vendor/isotope/js/isotope.pkgd.min.js"           type="text/javascript"></script>
<script src="assets/vendor/mfp/js/jquery.magnific-popup.min.js"      type="text/javascript"></script>
<script src="assets/vendor/circle-progress/circle-progress.js"       type="text/javascript"></script>
<script src="assets/vendor/waypoints/waypoints.min.js"               type="text/javascript"></script>
<script src="assets/vendor/anicounter/jquery.counterup.min.js"       type="text/javascript"></script>
<!--<script src="assets/vendor/wow/wow.min.js"                           type="text/javascript"></script>-->
<script src="assets/vendor/pjax/jquery.pjax.js"                      type="text/javascript"></script>
<!-- Custom scripts -->
<script src="assets/custom/js/custom.js"                             type="text/javascript"></script>

</body>

</html>