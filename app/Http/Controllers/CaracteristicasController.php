<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Mail;

class CaracteristicasController extends Controller
{
    public function feedback()
    {
        return view('feedback');
    }
    
    public function enviaFeedback(Request $request)
    {
        
        $this->nome = $request->nome;
        $this->email = $request->email;
        $this->mensagem = $request->mensagem;
        
        Mail::send('emails.mensagem', ['nome'=>$this->nome, 'email'=>$this->email, 'mensagem'=>nl2br($this->mensagem)], function ($message) {
            $message->from('contato@felipepastana.com', $this->nome);

            $message->to('contato@felipepastana.com', 'Felipe Pastana');
            
            $message->subject('Mensagem recebida felipepastana.com');
        });
        
        return redirect('feedback')->with('status', 'Mensagem enviada com sucesso!');
    }
}
