<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('curriculo', function () {
    return view('curriculo');
});

Route::get('portfolio', function () {
    return view('portfolio');
});

Route::get('contatos', function () {
    return view('contatos');
});

Route::get('feedback', 'CaracteristicasController@feedback');
Route::post('feedback', 'CaracteristicasController@enviaFeedback');

Route::get('blog', function () {
    return view('blog');
});
